flask
pymysql
flask-sqlalchemy
flask-script
flask-wtf
wtforms-components
flask-migrate
py-bcrypt
python-slugify
flask-markdown
flask-excel
-e git+https://github.com/fromzeroedu/flask_uploads.git#egg=flask_uploads
pygal
colour