from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.migrate import Migrate
from flaskext.markdown import Markdown
from flask_uploads.uploads import UploadSet, configure_uploads, IMAGES
app = Flask(__name__)
app.config.from_object('settings')
db = SQLAlchemy(app)


#migrations
migrate = Migrate(app , db)

#markdown
Markdown(app)

#images
uploaded_images = UploadSet('images', IMAGES)
configure_uploads(app, uploaded_images)


from home import views 
from admin import views
from modules.crimetypes import views
from modules.barangays import views
from modules.populations import views

from modules.cases import views