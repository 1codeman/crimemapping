from crime_mapping import db, uploaded_images
from datetime import datetime

class Blog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    admin = db.Column(db.Integer, db.ForeignKey('author.id'))
    post = db.relationship('Post', backref='blog', lazy='dynamic')
    
    def __init__(self, name, admin):
        self.name = name
        self.admin = admin
        
    def __repr__(self):
        return '<Blog %r>' % (self.name)

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    blog_id = db.Column(db.Integer, db.ForeignKey('blog.id'))
    author_id = db.Column(db.Integer, db.ForeignKey('author.id'))
    title = db.Column(db.String(80))
    body = db.Column(db.Text)
    image = db.Column(db.String(255))
    slug = db.Column(db.String(256), unique=True)
    publish_date = db.Column(db.DateTime)
    live = db.Column(db.Boolean)


    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))    
    category = db.relationship('Category', backref=db.backref('posts', lazy='dynamic'))
    
    @property
    def imgsrc(self):
        return uploaded_images.url(self.image)
        
    def __init__(self, blog, author, title, body, category, image=None, slug=None, publish_date=None, live=True):
        self.blog_id = blog.id
        self.author_id = author.id
        self.title = title
        self.body = body
        self.category_id = category.id
        self.image = image
        self.slug = slug
        if publish_date is None:
            self.publish_date = datetime.utcnow()
        else:
            publish_date = publish_date
        self.live = live
    
    def __repr__(self):
        return '<Post %r>' % self.title
    
        
class Category(db.Model):
        
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    
    def __init__(self, name):
            self.name = name
            
    def __repr__(self):
        return self.name

class Crime(db.Model):
    id = db.Column(db.String(20), primary_key=True)
    crime = db.Column(db.String(100))
    crime_type = db.Column(db.String(80))
    crime_info = db.Column(db.String(600))
    crime_date = db.Column(db.DateTime)
    address =  db.Column(db.String(80))
    barangay = db.Column(db.String(80))
    lon =  db.Column(db.String(50))
    lat =  db.Column(db.String(50))
    
  
    def __init__(self, id,crime, crime_type,crime_info,crime_date,address,barangay,lon,lat ):
            self.id = id
            self.crime = crime
            self.crime_type = crime_type
            self.crime_info = crime_info
            self.crime_date = crime_date
            self.address = address
            self.barangay = barangay
            self.lon = lon
            self.lat = lat
            
        
    def __repr__(self):
        return '<Crime %r>' % self.crime


class Cases(db.Model):
    
    id = db.Column(db.String(20), primary_key=True)
    crime = db.Column(db.String(100))
    crime_type = db.Column(db.String(80))
    crime_info = db.Column(db.String(600))
    crime_date = db.Column(db.DateTime)
    address =  db.Column(db.String(80))
    barangay = db.Column(db.String(80))
    lon =  db.Column(db.String(50))
    lat =  db.Column(db.String(50))
    
    barangay_id = db.Column(db.Integer, db.ForeignKey('barangay.id'))
    crimetype_id = db.Column(db.Integer, db.ForeignKey('crime_type.id'))
    judgement_date = db.Column(db.DateTime)
    case_info = db.Column(db.String(100))
    
    def __init__(self,crime_no,crime, crime_type,crime_info,crime_date,address,barangay,lon,lat,barangay_id,crimetype_id,judgement_date,case_info):
            self.id = crime_no
            self.crime = crime
            self.crime_type = crime_type
            self.crime_info = crime_info
            self.crime_date = crime_date
            self.address = address
            self.barangay = barangay
            self.lon = lon
            self.lat = lat
            self.barangay_id = barangay_id
            self.crimetype_id = crimetype_id
            self.judgement_date = judgement_date
            self.case_info = case_info
            
        
    def __repr__(self):
        return '<Case %r>' % self.case_info
        
class Population(db.Model):
        
    id = db.Column(db.Integer, primary_key=True)
    population =db.Column(db.Integer)
    year =db.Column(db.Integer)
    barangay = db.Column(db.String(50))
    barangay_id = db.Column(db.Integer, db.ForeignKey('barangay.id'))
    
    def __init__(self, population,barangay,barangay_id,year):
            self.population = population
            self.barangay = barangay
            self.barangay_id = barangay_id
            self.year = year
            
            
    def __repr__(self):
        return self.population