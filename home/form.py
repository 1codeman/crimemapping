from flask_wtf import Form
from crime_mapping import db
from wtforms import validators, StringField, TextAreaField
from admin.form import RegisterForm
from home.models import Category, Crime,Cases
from modules.crimetypes.models import  CrimeType
from modules.barangays.models import  Barangay
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms import SelectField
from flask_wtf.file import FileField, FileAllowed
from flask_uploads.uploads import UploadSet, configure_uploads, IMAGES
from sqlalchemy import extract


#form for the setup of site (setting site name and admin)
class SetupForm(RegisterForm):
    name = StringField('Site name', [
        validators.Required(),
        validators.Length(max=80)
        ])

class CategoryForm(Form):
    name = StringField('Category name', [
        validators.Required(),
        validators.Length(max=80)
        ]) 
        
class CrimeTypeForm(Form):
    name = StringField('Crime Type Name', [
        validators.Required(),
        validators.Length(max=80)
        ]) 
        
        
def categories():
    return Category.query
    
class PostForm(Form):
    image = FileField('Image', validators=[
        FileAllowed(['jpg', 'png'], 'Images only!')
        ])
    title = StringField('Title', [
        validators.Required(),
        validators.Length(max=80)
        ])
    body = TextAreaField('Content', validators=[validators.Required()])
    category = QuerySelectField('Category', query_factory=categories, allow_blank=True)
    new_category = StringField('New Category')   
    
class ImportForm(Form):
    csv = FileField('Csv File',  validators=[
        validators.Required(),
        FileAllowed(['csv'], 'Csv only!')
        ])
        
class SiteNameForm(Form):
    name = StringField('Site name', [
        validators.Required(),
        validators.Length(max=80)
        ])
        
def crime_type():
    return CrimeType.query.order_by(CrimeType.name.asc()).all()

def barangay():
    return Barangay.query.order_by(Barangay.name.asc()).all()
    

    
class FilterMapForm(Form):
    arr = list(Cases.query.order_by(Cases.crime_date.asc()).all())
    arr2 = []
    for x in arr:
        print x.crime_date.year
        if x.crime_date.year not in arr2:
            arr2.append(x.crime_date.year)
            print x.crime_date.year
    
    
    
    crime_month = SelectField("Month: ", choices =  [("", ""),(1, "January"),(2, "February"),(3,"March"),(4,"April"),(5,"May"),
    (6, "June"),(7,"July"),(8,"August"),(9,"September"),(10,"October"),(11,"November"),(12,"December")],default = "")        
    
    crime_year = SelectField("Year: ", choices =  [(a, a) for a in arr2],default = a)
    crime_types = QuerySelectField('Crime', query_factory=crime_type, allow_blank=True)
    barangays = QuerySelectField('Barangay  ', query_factory=barangay, allow_blank=True)

    
class FilterGraphForm(Form):
    arr = list(Cases.query.order_by(Cases.crime_date.asc()).all())
    arr2 = []

    for x in arr:
        if x.crime_date.year not in arr2:
            arr2.append(x.crime_date.year)
            
    crime_year = SelectField("Year: ", choices =  [(a, a) for a in arr2],default = a)

class FilterHistographForm(Form):
    arr = list(Cases.query.order_by(Cases.crime_date.asc()).all())
    arr2 = []
    arr2.append(None)
    for x in arr:
        if x.crime_date.year not in arr2:
            arr2.append(x.crime_date.year)
            
    crime_year = SelectField("Year: ", choices =  [(a, a) for a in arr2],default = "")
    barangays = QuerySelectField('Barangay  ', query_factory=barangay, allow_blank=True)