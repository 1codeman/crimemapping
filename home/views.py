from crime_mapping import app
from flask.ext import excel
from werkzeug import secure_filename
from flask import render_template, redirect, flash, url_for, session, abort, request,jsonify
from home.form import SetupForm, PostForm , ImportForm, SiteNameForm, CategoryForm, FilterMapForm, FilterGraphForm,FilterHistographForm
from crime_mapping import db, uploaded_images
from admin.models import Author
from home.models import Blog, Post, Category, Crime,Population,Cases
from modules.barangays.models import Barangay
from modules.crimetypes.models import CrimeType
from admin.decorators import login_required, author_required,noblog_required,blog_required
import bcrypt, sys
from slugify import slugify
import pygal 
import datetime
from datetime import datetime
from sqlalchemy import extract, func


POSTS_PER_PAGE = 3

@app.route('/')
@app.route('/index')
@app.route('/index/<int:page>')
@blog_required
def index(page=1):
    blog = Blog.query.first()
    if not blog:
        return redirect(url_for('setup'))
    posts = Post.query.filter_by(live=True).order_by(Post.publish_date.desc()).paginate(page,POSTS_PER_PAGE,False)
    return render_template('home/index.html',blog=blog, posts=posts)
    
    
@app.route('/admin')
@app.route('/admin/<int:page>')
@blog_required
@author_required
def admin(page=1):
    blog = Blog.query.first()
    if session.get('is_author'):
        posts = Post.query.order_by(Post.publish_date.desc()).paginate(page,POSTS_PER_PAGE,False)
        return render_template('home/admin.html', blog=blog, posts=posts)
    else:
        abort(403)
  

@app.route('/admin/manage',methods=('GET','POST'))    
@app.route('/admin/manage/<int:page>',methods=('GET','POST'))
@blog_required
@author_required
def manage(page=1):
    form = SiteNameForm()
    blog = Blog.query.first()
    if form.validate_on_submit():
       blog.name = form.name.data
       db.session.commit()
       flash("Site name has been changed!")
       form.name.data=""
       
     
    posts = Post.query.order_by(Post.publish_date.desc()).paginate(page,5,False)
    return render_template('home/manage.html', blog=blog, posts=posts,form=form)
    
@app.route('/admin/manage/category',methods=('GET','POST'))    
@app.route('/admin/manage/category/<int:page>',methods=('GET','POST'))
@blog_required
@author_required
def managecategory(page=1):
    form = CategoryForm()
    blog = Blog.query.first()
    
    if form.validate_on_submit():
       new_category = Category(form.name.data)
       db.session.add(new_category)
       db.session.commit()
       flash("New Category has been added!")
       form.name.data=""
       return redirect('admin/manage/category')
    categories = Category.query.order_by(Category.id.desc()).paginate(page,5,False)
    return render_template('home/managecategory.html', blog=blog, categories=categories,form=form)
    

@app.route('/setup', methods=('GET','POST'))
@noblog_required
def setup():
    form = SetupForm()
    error = ""
    if form.validate_on_submit():
        salt = bcrypt.gensalt()
        hashed_password = bcrypt.hashpw(form.password.data,salt)
        author = Author(
            form.fullname.data,
            form.email.data,
            form.username.data,
            hashed_password,
            True
            )
        db.session.add(author)
        db.session.flush()
        if author.id:
            blog = Blog(
                form.name.data,
                author.id
                )
            db.session.add(blog)
            db.session.flush()
        else:
            db.session.rollback()
            error = "Error creating user"
        if author.id and blog.id:
            db.session.commit()
            flash("Site Created")
            return redirect(url_for('login'))
        else:
            db.session.rollback()
            error = "Error creating site"
        
    return render_template('home/setup.html',form=form, error = error)
    
@app.route('/post', methods=('GET','POST'))
@blog_required
@author_required
def post():
    blog = Blog.query.first()
    form = PostForm()
    if form.validate_on_submit():
        image = request.files.get('image')
        filename = None
        if form.image.has_file():
            try:
                filename = uploaded_images.save(image)
            except:
                flash("The image was not uploaded") 
        if form.new_category.data:
            new_category = Category(form.new_category.data)
            db.session.add(new_category)
            db.session.flush()
            category = new_category
        elif form.category.data:
            category_id = form.category.get_pk(form.category.data)
            category= Category.query.filter_by(id=category_id).first()
        else:
            category = None
        blog = Blog.query.first()
        author = Author.query.filter_by(username= session['username']).first()
        title = form.title.data
        body = form.body.data
        slug = slugify(title)
        post = Post(blog, author, title, body, category, filename, slug)
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('article',slug=slug))
    return render_template('home/post.html', blog=blog,form=form, action = "new")
 
        
@app.route('/article/<slug>', methods=('GET', 'POST'))
@blog_required
def article(slug):
    blog = Blog.query.first()
    post = Post.query.filter_by(slug=slug).first_or_404()
    return render_template('home/article.html',blog=blog,post=post)


@app.route('/edit/<int:post_id>', methods=("GET","POST"))
@blog_required
@author_required
def edit(post_id):
    blog = Blog.query.first()
    post = Post.query.filter_by(id=post_id).first_or_404()
    form = PostForm(obj=post)
    if form.validate_on_submit():
        original_image = post.image
        form.populate_obj(post)
        if form.image.has_file():
            image = request.files.get('image')
            try:
                filename = uploaded_images.save(image)
            except:
                flash("The image was not uploaded")
            if filename:
                post.image = filename
        else:
            post.image = original_image
        if form.new_category.data:
            new_category = Category(form.new_category.data)
            db.session.add(new_category)
            db.session.flush()
            post.category = new_category
        db.session.commit()
        return redirect(url_for('article', slug = post.slug))
    return render_template('home/post.html', blog=blog,form=form, post=post, action='edit')
    
    
@app.route('/deletecategory/<int:cat_id>')
@blog_required
@author_required
def deletecategory(cat_id):
    
    try:
        Category.query.filter_by(id=cat_id).delete()
        db.session.commit()
        flash("Category deleted")
        blog = Blog.query.first()
        categories = Category.query.order_by(Category.id.desc()).paginate(page,5,False)
        return render_template('home/managecategory.html', blog=blog, categories=categories,form=form)
    except:
        return redirect('admin/manage/category')
        
    
    
  

    
@app.route('/delete/<int:post_id>')
@blog_required
@author_required
def delete(post_id):
    post= Post.query.filter_by(id=post_id).first_or_404()
    post.live=False
    db.session.commit()
    flash("Article deleted")
    return redirect('/admin/manage')

   
@app.route('/retrieve/<int:post_id>')
@blog_required
@author_required
def retrieve(post_id):
    post= Post.query.filter_by(id=post_id).first_or_404()
    post.live=True
    db.session.commit()
    flash("Successfully Retrieve Article ")
    return redirect('/admin/manage')
    
    

@app.route('/map/<number>/')
@blog_required
def detail(number):
    blog = Blog.query.first()
    query_sets  = Cases.query.filter_by(id=number).first()
    return render_template('home/detail.html',
        object=query_sets,blog=blog
    )


@app.route('/import', methods=('GET','POST'))
@blog_required
@author_required
def csvtomysql():
    prompt = None
    blog = Blog.query.first()
    form = ImportForm()
    csv = request.files.get('csv')
    # x =  datetime.datetime('1/15/2016 23:38')

    dt = datetime.strptime('1/15/2016 23:38', '%m/%d/%Y %H:%M')
   
    if form.validate_on_submit() and  request.form['btn'] == 'Import Crimes':
         prompt = "Import Crimes"
         csvfile = request.get_records(field_name='csv',encoding='utf-8', errors='ignore')
         print csvfile
         
         try :
               
            for crime in csvfile :
                #check if the crime  is already in the database
                isExisted = Cases.query.filter_by(id=crime['Blotter Number']).first()
              
                if isExisted :
                      # if existed? update it
                      isExisted.crime = crime['Stages of Felony']
                      isExisted.crime_type = crime['Offense']
                      isExisted.crime_info = crime['Narrative']
                    #   datetime.strptime(crime['crime_date'], '%m/%d/%Y %H:%M')
                        # print datetime.strptime(crime['Date Committed'], '%m/%d/%Y')
                      isExisted.crime_date =  datetime.strptime(crime['Date Committed'], '%m/%d/%Y')
                      isExisted.address = ""
                      isExisted.barangay = crime['Barangay']
                      isExisted.lon = crime['Longtitude']
                      isExisted.lat = crime['Latitude']
                      db.session.commit()
                      
                else :
                       print datetime.strptime(crime['Date Committed'], '%m/%d/%Y')
                      # add it to the database
                       newCrime = Crime(crime['Blotter Number'], crime['Stages of Felony'],crime['Offense'],crime['Narrative'],datetime.strptime(crime['Date Committed'], '%m/%d/%Y'),"",crime['Barangay'],crime['Longtitude'],crime['Latitude'])
                       db.session.add(newCrime)
                       db.session.commit()
    
            flash("Successfully Imported Data")
            return render_template('home/import.html', blog=blog,form=form, prompt=prompt)
                
         except Exception as e:
                flash(e)
                flash("Unable to import data")
       
    ########################################### IMPORT BARANGAY ##################################################
    if form.validate_on_submit() and  request.form['btn'] == 'Import Barangays':
         prompt = "Import Barangays"
         csvfile = request.get_records(field_name='csv')
         try :
            #delete all barangay rows
            Barangay.query.delete()
            for barangay in csvfile :
                  # add it to the database
                   newBarangay = Barangay(barangay['name'],barangay['latitude'],barangay['longitude'])
                   db.session.add(newBarangay)
                   db.session.commit()
                       
            flash("Successfully Imported Barangay Data")
            return render_template('home/import.html', blog=blog,form=form,prompt=prompt)
                
         except Exception as e:
                flash(e)
                flash("Unable to import data")
                
    ########################################### IMPORT CRIME TYPES ##################################################
    if form.validate_on_submit() and  request.form['btn'] == 'Import Types':
         prompt = "Import Types"
         csvfile = request.get_records(field_name='csv')
         try :
            #delete all barangay rows
            CrimeType.query.delete()
            for crimetype in csvfile :
                  # add it to the database
                   newType = CrimeType( crimetype['name'],crimetype['hexcolor'])
                   db.session.add(newType)
                   db.session.commit()
                       
            flash("Successfully Imported Crime Type Data")
            return render_template('home/import.html', blog=blog,form=form,prompt=prompt)
                
         except Exception as e:
                flash(e)
                flash("Unable to import data")   
                
                
     ########################################### IMPORT POPULATIONS ##################################################
    if form.validate_on_submit() and  request.form['btn'] == 'Import Populations':
         prompt = "Import Populations"
         csvfile = request.get_records(field_name='csv')
         try :
            #delete all population rows
            Population.query.delete()
            for population in csvfile :
                  # add it to the database
                   newPopulation = Population( population['population'],population['barangay'])
                   db.session.add(newPopulation)
                   db.session.commit()
                       
            flash("Successfully Imported Populations Data")
            return render_template('home/import.html', blog=blog,form=form,prompt=prompt)
                
         except Exception as e:
                flash(e)
                flash("Unable to import data")            
                            
    return render_template('home/import.html', blog=blog,form=form)
    

def getcrimes():
    crimes = Cases.query.all()
    count =len(crimes)
    return count
    

@app.route('/map', methods=('GET','POST'))
@app.route('/map')
@blog_required
def mapbox():
    count = 0
    crimes = None
    form = FilterMapForm()
    blog = Blog.query.first()
    isBarangay = False
    lat = None
    lon = None
    crime_types = CrimeType.query.all()
    if request.method == 'POST':
        if (form.barangays.data  != None) and (form.crime_types.data != None): 
            isBarangay = True
            barangay = Barangay.query.filter_by(name = form.barangays.data.name).all()
            if form.crime_month.data == "":
                crimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter_by(barangay=form.barangays.data.name).filter_by(crime_type=form.crime_types.data.name).all()
            else:
                crimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter(extract('month', Cases.crime_date) == form.crime_month.data).filter_by(barangay=form.barangays.data.name).filter_by(crime_type=form.crime_types.data.name).all()
            
            count = len(crimes)
            pie_chart = pygal.Gauge(width=500,height=500,show_legend=False)
            
            if form.crime_month.data == "":
                xcrimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).all()
            else:
                xcrimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter(extract('month', Cases.crime_date) == form.crime_month.data).all()
                
            xcount =len(xcrimes)
            pie_chart.range = [0, xcount]
            pie_chart.title = form.crime_types.data.name + " Gauge in " + form.barangays.data.name  + " (" + form.crime_year.data + ")"
            pie_chart.add(form.crime_types.data.name, count)
            pie_chart.render()
            graph_data = pie_chart.render_data_uri()
        elif(form.barangays.data  != None): 
            isBarangay = True
            barangay = Barangay.query.filter_by(name = form.barangays.data.name).all()
            if form.crime_month.data == "":
               crimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter_by(barangay=form.barangays.data.name).all()
            else:
               crimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter(extract('month',Cases.crime_date) == form.crime_month.data).filter_by(barangay=form.barangays.data.name).all()
            
            count = len(crimes)
        
            crime_type_list = []
            barangay_list = []
            crime_types = CrimeType.query.order_by(CrimeType.name.asc()).all()
            barangays = Barangay.query.order_by(Barangay.name.asc()).all()
            for xbarangay in barangays:
                barangay_list.append(xbarangay)
            
            pie_chart = pygal.Pie()
            pie_chart.title = 'All Crimes in Barangay ' + form.barangays.data.name + " ("+ form.crime_month.data  + form.crime_year.data + ")"
            pie_chart.x_labels = map(str,barangay_list)
            crime_count_list = []
            for crime in crime_types:
                crime_count_list = []
                if form.crime_month.data == "":
                    xcrimes =  Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter_by(crime_type=crime.name).filter_by(barangay=form.barangays.data.name).all()
                else:
                    xcrimes =  Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter(extract('month',Cases.crime_date) == form.crime_month.data).filter_by(crime_type=crime.name).filter_by(barangay=form.barangays.data.name).all()
                xcount=len(xcrimes)
                crime_count_list.append(xcount)
                pie_chart.add(crime.name, crime_count_list)
            pie_chart.render()
            graph_data = pie_chart.render_data_uri()
            
        elif(form.crime_types.data != None): 
            isBarangay == False
            barangay = None
            if form.crime_month.data == "":
                crimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter_by(crime_type=form.crime_types.data.name).all()
            else:
                crimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter(extract('month',Cases.crime_date) == form.crime_month.data).filter_by(crime_type=form.crime_types.data.name).all()
            count = len(crimes)
            pie_chart = pygal.Gauge(width=500,height=500,show_legend=False)
            
            if form.crime_month.data == "":
                xcrimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).all()
            else:
                xcrimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter(extract('month',Cases.crime_date) == form.crime_month.data).all()
                
            xcount =len(xcrimes)
            print xcount
            pie_chart.range = [0, xcount]
            pie_chart.title = form.crime_types.data.name + " Gauge in All Barangays " + "(" + form.crime_month.data + form.crime_year.data + ")"
            pie_chart.add("", count)
            pie_chart.render()
            graph_data = pie_chart.render_data_uri()
        else:
            if form.crime_month.data == "":
                crimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).all()
            else:
                crimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter(extract('month',Cases.crime_date) == form.crime_month.data).all()
               
            
            count = len(crimes)
            
            crime_type_list = []
            barangay_list = []
            crime_types = CrimeType.query.order_by(CrimeType.name.asc()).all()
            barangays = Barangay.query.order_by(Barangay.name.asc()).all()
            for barangay in barangays:
                barangay_list.append(barangay)
            
            pie_chart = pygal.Pie()
            pie_chart.title = 'All Crimes ' + "(" + form.crime_month.data + form.crime_year.data + ")" 
            pie_chart.x_labels = map(str,barangay_list)
            crime_count_list = []
            for crime in crime_types:
                crime_count_list = []
                if form.crime_month.data == "":
                    xcrimes =  Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter_by(crime_type=crime.name).all()
                else:
                    xcrimes =  Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter(extract('month',Cases.crime_date) == form.crime_month.data).filter_by(crime_type=crime.name).all()
                xcount=len(xcrimes)
                crime_count_list.append(xcount)
                pie_chart.add(crime.name, crime_count_list)
            pie_chart.render()
            graph_data = pie_chart.render_data_uri()
        return render_template('home/mapbox.html',blog=blog, object_list=crimes,
                               form=form,count=count,crime_types=crime_types,graph_data=graph_data,isBarangay=isBarangay,barangay=barangay)
    else:
        crimes = Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).all()
        count = len(crimes)
        
        crime_type_list = []
        barangay_list = []
        crime_types = CrimeType.query.order_by(CrimeType.name.asc()).all()
        barangays = Barangay.query.order_by(Barangay.name.asc()).all()
        for barangay in barangays:
            barangay_list.append(barangay)
        
        pie_chart = pygal.Pie()
        pie_chart.title = 'All Crimes ' + "(" + form.crime_year.data + ")"
        pie_chart.x_labels = map(str,barangay_list)
        crime_count_list = []
        for crime in crime_types:
            crime_count_list = []
            xcrimes =  Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter_by(crime_type=crime.name).all()
            xcount=len(xcrimes)
            crime_count_list.append(xcount)
            pie_chart.add(crime.name, crime_count_list)
        pie_chart.render()
        graph_data = pie_chart.render_data_uri()
        return render_template('home/mapbox.html',blog=blog, object_list=crimes,
                               form=form,count=count,crime_types=crime_types,graph_data = graph_data,isBarangay = False)
  
    

@app.route('/graph', methods=('GET','POST'))
@app.route('/graph')
@blog_required
def graph():
    blog = Blog.query.first()
    form = FilterGraphForm()
    crime_type_list = []
    barangay_list = []
    crime_types = CrimeType.query.order_by(CrimeType.name.asc()).all()
    barangays = Barangay.query.order_by(Barangay.name.desc()).all()
    for barangay in barangays:
        barangaycrimes =  Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter_by(barangay=barangay.name).all()
        barangay_list.append(barangay.name + "(" + str(len(barangaycrimes)) + ")")
    
    allcrimes =  Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).all()
    line_chart = pygal.HorizontalBar(width=1000, height=1500)
    line_chart.title = 'Crime Graph for the year ' + form.crime_year.data + " (with a total of " + str(len(allcrimes)) + " crimes)"
    line_chart.x_labels = map(str,barangay_list)
    crime_count_list = []
    for crime in crime_types:
        crime_count_list = []
        for barangay in barangays:
            crimes =  Cases.query.filter(extract('year', Cases.crime_date) == form.crime_year.data).filter_by(barangay=barangay.name).filter_by(crime_type=crime.name).all()
            count=len(crimes)
            crime_count_list.append(count)
        line_chart.add(crime.name, crime_count_list)
       
      
    graph_data = line_chart.render_data_uri()
    
    
    return render_template('home/graph.html',blog=blog,graph_data=graph_data,form=form)
    
@app.route('/histograph', methods=('GET','POST'))
@app.route('/histograph')
@blog_required
def histograph():
    blog = Blog.query.first()
    form = FilterHistographForm()
    crime_types = CrimeType.query.order_by(CrimeType.name.asc()).all()
    barangays = Barangay.query.order_by(Barangay.name.desc()).all()
    arr = list(Cases.query.order_by(Cases.crime_date.asc()).all())
    arr2 = []

    for x in arr:
        if x.crime_date.year not in arr2:
            arr2.append(x.crime_date.year)
    
    if request.method == 'POST':
        if (form.crime_year.data  != None and form.barangays.data  != None and form.crime_year.data != "None"):
            line_chart = pygal.Line()
            line_chart.title = 'Crime Evolution in Barangay ' + str(form.barangays.data.name) + ' (Year  ' + form.crime_year.data + ')'
            line_chart.x_labels = map(str, ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'])
            
            months = [1,2,3,4,5,6,7,8,9,10,11,12]
            crime_count_list = []
            for ctype in crime_types:
                crime_count_list = []
                for m in months:
                    crimes =  Cases.query.filter(extract('month', Cases.crime_date) ==m).filter(extract('year', Cases.crime_date) ==form.crime_year.data ).filter_by(crime_type=ctype.name).filter_by(barangay=form.barangays.data.name).all()
                    count=len(crimes)
                    crime_count_list.append(count)
                line_chart.add(ctype.name,crime_count_list)
                  
            graph_data = line_chart.render_data_uri()
        elif (form.crime_year.data  != None and form.crime_year.data != "None"):
            line_chart = pygal.Line()
            line_chart.title = 'Crime Evolution in Naga City (Year  ' + form.crime_year.data + ')'
            line_chart.x_labels = map(str, ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'])
            
            months = [1,2,3,4,5,6,7,8,9,10,11,12]
            crime_count_list = []
            for ctype in crime_types:
                crime_count_list = []
                for m in months:
                    crimes =  Cases.query.filter(extract('month', Cases.crime_date) ==m).filter(extract('year', Cases.crime_date) ==form.crime_year.data ).filter_by(crime_type=ctype.name).all()
                    count=len(crimes)
                    crime_count_list.append(count)
                line_chart.add(ctype.name,crime_count_list)
                  
            graph_data = line_chart.render_data_uri()
        elif (form.barangays.data  != None ): 
            line_chart = pygal.Line()
            line_chart.title = 'Crime Evolution in Barangay ' + form.barangays.data.name
            line_chart.x_labels = map(str, arr2)
    
            crime_count_list = []
            for ctype in crime_types:
                crime_count_list = []
                for a in arr2:
                    crimes =  Cases.query.filter(extract('year', Cases.crime_date) == a).filter_by(barangay=form.barangays.data.name).filter_by(crime_type=ctype.name).all()
                    count=len(crimes)
                    crime_count_list.append(count)
                line_chart.add(ctype.name,crime_count_list)
                  
            graph_data = line_chart.render_data_uri()
        else:
    
            line_chart = pygal.Line()
            line_chart.title = 'Crime Evolution in Naga City'
            line_chart.x_labels = map(str, arr2)
    
            crime_count_list = []
            for ctype in crime_types:
                crime_count_list = []
                for a in arr2:
                    crimes =  Cases.query.filter(extract('year', Cases.crime_date) == a).filter_by(crime_type=ctype.name).all()
                    count=len(crimes)
                    crime_count_list.append(count)
                line_chart.add(ctype.name,crime_count_list)
                  
            graph_data = line_chart.render_data_uri()
    
        
    else:    
        line_chart = pygal.Line()
        line_chart.title = 'Crime Evolution in Naga City'
        line_chart.x_labels = map(str, arr2)
        
        crime_count_list = []
        for ctype in crime_types:
            crime_count_list = []
            for a in arr2:
                crimes =  Cases.query.filter(extract('year', Cases.crime_date) == a).filter_by(crime_type=ctype.name).all()
                count=len(crimes)
                crime_count_list.append(count)
            line_chart.add(ctype.name,crime_count_list)
              
        graph_data = line_chart.render_data_uri()
        
        
    return render_template('home/histograph.html',blog=blog,graph_data=graph_data,form=form)

@app.route('/statistics', methods=('GET','POST'))
@app.route('/statistics')
@blog_required
def statistics():
    blog = Blog.query.first()
    form = FilterHistographForm()
    crime_types = CrimeType.query.order_by(CrimeType.name.asc()).all()
    barangays = Barangay.query.order_by(Barangay.name.desc()).all()
    arr = list(Cases.query.order_by(Cases.crime_date.asc()).all())
    arr2 = []

    for x in arr:
        if x.crime_date.year not in arr2:
            arr2.append(x.crime_date.year)
    
    if request.method == 'POST':
        if (form.crime_year.data  != None and form.barangays.data  != None and form.crime_year.data != "None"):
            barangay_population= db.session.query(func.sum(Population.population).label('total')).filter_by(barangay = form.barangays.data.name).scalar()
            line_chart = pygal.Line()
            line_chart.title = 'Crime Statistics in Barangay ' + str(form.barangays.data.name) + ' (per 100,000 population) ' +' (Year  ' + form.crime_year.data + ')'
            line_chart.x_labels = map(str, ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'])
            
            months = [1,2,3,4,5,6,7,8,9,10,11,12]
            crime_count_list = []
            for ctype in crime_types:
                crime_count_list = []
                for m in months:
                    crimes =  Cases.query.filter(extract('month', Cases.crime_date) ==m).filter(extract('year', Cases.crime_date) ==form.crime_year.data ).filter_by(crime_type=ctype.name).filter_by(barangay=form.barangays.data.name).all()
                    count=len(crimes)
                    count = (count/barangay_population)*100000
                    crime_count_list.append(round(count,2))
                line_chart.add(ctype.name,crime_count_list)
                  
            graph_data = line_chart.render_data_uri()
        elif (form.crime_year.data  != None and form.crime_year.data != "None"):
            
            total_population=db.session.query(func.sum(Population.population).label('total')).scalar()
            line_chart = pygal.Line()
            line_chart.title = 'Crime Statistics in Naga City (per 100,000 population) (Year  ' + form.crime_year.data + ')'
            line_chart.x_labels = map(str, ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'])
            
            months = [1,2,3,4,5,6,7,8,9,10,11,12]
            crime_count_list = []
            for ctype in crime_types:
                crime_count_list = []
                for m in months:
                    crimes =  Cases.query.filter(extract('month', Cases.crime_date) ==m).filter(extract('year', Cases.crime_date) ==form.crime_year.data ).filter_by(crime_type=ctype.name).all()
                    count=len(crimes)
                    count = (count/total_population)*100000
                    crime_count_list.append(round(count,2))
                line_chart.add(ctype.name,crime_count_list)
                  
            graph_data = line_chart.render_data_uri()
        elif (form.barangays.data  != None ): 
            
            
            barangay_population= db.session.query(func.sum(Population.population).label('total')).filter_by(barangay = form.barangays.data.name).scalar()
            line_chart = pygal.Bar()
            line_chart.title = 'Crime Statistics in Barangay ' + form.barangays.data.name + " (per 100,000 population)"
            line_chart.x_labels = map(str, arr2)
    
            crime_count_list = []
            for ctype in crime_types:
                crime_count_list = []
                for a in arr2:
                    crimes =  Cases.query.filter(extract('year', Cases.crime_date) == a).filter_by(barangay=form.barangays.data.name).filter_by(crime_type=ctype.name).all()
                    count=len(crimes)
                    count = (count/barangay_population)*100000
                    crime_count_list.append(round(count,2))
                line_chart.add(ctype.name,crime_count_list)
                  
            graph_data = line_chart.render_data_uri()
        else:
    
            total_population=db.session.query(func.sum(Population.population).label('total')).scalar()
            line_chart = pygal.Bar()
            line_chart.title = 'Crime Statistics in Naga City (per 100,000 population)'
            line_chart.x_labels = map(str, arr2)
    
            crime_count_list = []
            for ctype in crime_types:
                crime_count_list = []
                for a in arr2:
                    crimes =  Cases.query.filter(extract('year', Cases.crime_date) == a).filter_by(crime_type=ctype.name).all()
                    count=len(crimes)
                    count = (count/total_population)*100000
                    crime_count_list.append(round(count,2))
                line_chart.add(ctype.name,crime_count_list)
                  
            graph_data = line_chart.render_data_uri()
    
        
    else:    
        total_population=db.session.query(func.sum(Population.population).label('total')).scalar()
        line_chart = pygal.Bar()
        line_chart.title = 'Crime Statistics in Naga City (per 100,000 population)'
        line_chart.x_labels = map(str, arr2)
        
        crime_count_list = []
        for ctype in crime_types:
            crime_count_list = []
            for a in arr2:
                crimes =  Cases.query.filter(extract('year', Cases.crime_date) == a).filter_by(crime_type=ctype.name).all()
                count=len(crimes)
                count = (count/total_population)*100000
                crime_count_list.append(round(count,2))
            line_chart.add(ctype.name,crime_count_list)
              
        graph_data = line_chart.render_data_uri()
        
        
    return render_template('home/statistics.html',blog=blog,graph_data=graph_data,form=form)