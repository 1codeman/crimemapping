from crime_mapping import app
from flask import render_template, redirect, url_for, session, request
from admin.form import RegisterForm, LoginForm
from admin.models import Author
from home.models import Blog
from admin.decorators import login_required, notlogin_required,blog_required
import bcrypt   

#Flask Pluggable Views Documentations:
#http://flask.pocoo.org/docs/0.10/views/

@app.route('/login', methods=('GET', 'POST'))
@blog_required
@notlogin_required
def login():
    blog = Blog.query.first()
    form = LoginForm()
    error = None
    
    if request.method == 'GET' and request.args.get('next'):
        session['next'] = request.args.get('next', None)
    if form.validate_on_submit():
        author = Author.query.filter_by(
            username=form.username.data,
            ).first()
        if author:
            if bcrypt.hashpw(form.password.data, author.password) == author.password:
                session['username'] = form.username.data
                session['is_author'] = author.is_author
                if 'next' in session:
                    next = session.get('next')
                    session.pop('next')
                    return redirect(next)
                else:
                    return redirect(url_for('index'))
            else:
                error = "Incorrect username and password"
        else:
            error = "Incorrect username and password"
    return render_template('admin/login.html', form=form, error=error, blog = blog)

@app.route('/register', methods=('GET', 'POST'))
@blog_required
@notlogin_required
def register():
    blog = Blog.query.first()
    form = RegisterForm()
    if form.validate_on_submit():
        return redirect(url_for('login'))
    return render_template('admin/register.html', form=form, blog=blog)

@app.route('/logout')
def logout():
    session.pop('username')
    session.pop('is_author')
    return redirect(url_for('index'))
