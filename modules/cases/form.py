#from wtforms import Form
from flask_wtf import Form

from crime_mapping import db

from wtforms import validators, StringField, TextAreaField,DateField,DateTimeField
from wtforms_components import ColorField
from admin.form import RegisterForm
from home.models import Category, Crime, Cases
from modules.barangays.models import Barangay
from modules.crimetypes.models import  CrimeType
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms import SelectField
from flask_wtf.file import FileField, FileAllowed
from flask_uploads.uploads import UploadSet, configure_uploads, IMAGES
from sqlalchemy import extract
from datetime import datetime


def crime_type():
    return CrimeType.query.order_by(CrimeType.name.asc()).all()

def barangay():
    return Barangay.query.order_by(Barangay.name.asc()).all()
    
        
class CaseForm(Form):
    case_info = TextAreaField('Case Info', [
        validators.Required(),
        validators.Length(max=300)
        ]) 
        
    case_no = StringField('Case No', [
    validators.Required(),
    validators.Length(max=300)
    ]) 
    
    crime_info = TextAreaField('Crime Info', [
    validators.Required(),
    validators.Length(max=300)
    ]) 
    
    
    address = TextAreaField('Crime Address', [
    validators.Required(),
    validators.Length(max=300)
    ]) 
        
    crime_date = DateField('Crime Date',[
        validators.Required(),
        ],format='%Y-%m-%d') 
    

    judgement_date = DateField('Judgement Date',[
        validators.Required(),
        ],format='%Y-%m-%d') 

    barangay = QuerySelectField('Barangay', query_factory=barangay)
    
    crime_type = QuerySelectField('Crime Type', query_factory=crime_type)
        
      
    lon = StringField('Longitude', [
    validators.Required(),
   
    ],default=123.20111277511586) 
    
    lat = StringField('Latitude', [
    validators.Required(),
    ],default=13.620212373651398) 