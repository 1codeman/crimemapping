from crime_mapping import app
from flask.ext import excel
from werkzeug import secure_filename
from flask import render_template, redirect, flash, url_for, session, abort, request,jsonify
from modules.cases.form import CaseForm
from crime_mapping import db, uploaded_images
from admin.models import Author
from home.models import Blog, Post, Category, Crime, Population,Cases
from modules.crimetypes.models import CrimeType
from modules.barangays.models import Barangay
from admin.decorators import login_required, author_required,noblog_required,blog_required
import bcrypt, sys
from slugify import slugify
import pygal 
import datetime
from datetime import datetime
from sqlalchemy import extract, func



    

    
@app.route('/admin/manage/cases',methods=('GET','POST'))    
@app.route('/admin/manage/cases/<int:page>',methods=('GET','POST'))
@blog_required
@author_required
def managecases(page=1):
    form = CaseForm()
    blog = Blog.query.first()
    
    if form.validate_on_submit():
      
      barangay_id = form.barangay.get_pk(form.barangay.data)
      barangay= Barangay.query.filter_by(id=barangay_id).first()
      crimetype_id = form.barangay.get_pk(form.crime_type.data)
      crimetype= CrimeType.query.filter_by(id=crimetype_id).first()
      
      new_crimetype = Cases(form.case_no.data,None,crimetype.name,form.crime_info.data,form.crime_date.data,form.address.data,barangay.name,form.lon.data,form.lat.data,barangay_id,crimetype_id,form.judgement_date.data,form.case_info.data)
      db.session.add(new_crimetype)
      db.session.commit()
      flash("New Crime has been added!")
      form.case_no.data=""
      form.crime_info.data=""
      form.address.data=""
      form.case_info.data=""
    
      cases = Cases.query.order_by(Cases.judgement_date.asc()).paginate(page,10,False)
      return redirect('admin/manage/cases')
    cases = Cases.query.order_by(Cases.judgement_date.asc()).paginate(page,10,False)
    return render_template('cases/index.html', blog=blog, cases=cases,form=form)
    
    
    
@app.route('/deletecase/<string:case_id>')
@blog_required
@author_required
def deletecase(case_id):
    
    try:
        Cases.query.filter_by(id=case_id).delete()
        db.session.commit()
        flash("Case deleted")
        blog = Blog.query.first()
        cases = Cases.query.order_by(Cases.judgement_date.asc()).paginate(page,10,False)
        return render_template('cases/index.html', blog=blog, cases=cases,form=form)
    except:
        return redirect('/admin/manage/cases')