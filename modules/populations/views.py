from crime_mapping import app
from flask.ext import excel
from werkzeug import secure_filename
from flask import render_template, redirect, flash, url_for, session, abort, request,jsonify
from modules.populations.form import PopulationForm
from crime_mapping import db, uploaded_images
from admin.models import Author
from home.models import Blog, Post, Category, Crime, Population
from modules.crimetypes.models import CrimeType
from modules.barangays.models import Barangay
from admin.decorators import login_required, author_required,noblog_required,blog_required
import bcrypt, sys
from slugify import slugify
import pygal 
import datetime
from datetime import datetime
from sqlalchemy import extract, func



    

    
@app.route('/admin/manage/populations',methods=('GET','POST'))    
@app.route('/admin/manage/populations/<int:page>',methods=('GET','POST'))
@blog_required
@author_required
def managepopulations(page=1):
    form = PopulationForm()
    blog = Blog.query.first()
    
    if form.validate_on_submit():
       barangay_id = form.barangay.get_pk(form.barangay.data)
       barangay= Barangay.query.filter_by(id=barangay_id).first()
       print form.population.data
       
       new_crimetype = Population(form.population.data,barangay.name,barangay_id,form.year.data)
       db.session.add(new_crimetype)
       db.session.commit()
       flash("New Population has been added!")
       form.population.data=""
       form.year.data =""
       populations = Population.query.order_by(Population.id.asc()).paginate(page,5,False)
       return redirect('admin/manage/populations')
    populations = Population.query.order_by(Population.id.asc()).paginate(page,5,False)
    return render_template('populations/managepopulations.html', blog=blog, populations=populations,form=form)
    
    
    
@app.route('/deletepopulation/<int:population_id>')
@blog_required
@author_required
def deletepopulation(population_id):
    
    try:
        Population.query.filter_by(id=population_id).delete()
        db.session.commit()
        flash("Population deleted")
        blog = Blog.query.first()
        populations = Population.query.order_by(Population.name.asc()).paginate(page,5,False)
        return render_template('barangays/managepopulations.html', blog=blog, populations=populations,form=form)
    except:
        return redirect('/admin/manage/populations')