#from wtforms import Form
from flask_wtf import Form

from crime_mapping import db

from wtforms import validators, StringField, TextAreaField
from wtforms_components import ColorField
from admin.form import RegisterForm
from home.models import Category, Crime
from modules.barangays.models import Barangay
from modules.crimetypes.models import  CrimeType
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms import SelectField
from flask_wtf.file import FileField, FileAllowed
from flask_uploads.uploads import UploadSet, configure_uploads, IMAGES
from sqlalchemy import extract



        
class BarangayForm(Form):
    name = StringField('Barangay Name', [
        validators.Required(),
        validators.Length(max=80)
        ]) 

        
    lon = StringField('Longitude', [
    validators.Required(),
   
    ],default=123.20111277511586) 
    
    lat = StringField('Latitude', [
    validators.Required(),
    ],default=13.620212373651398) 