from crime_mapping import db, uploaded_images
from datetime import datetime
from colour import Color


class CrimeType(db.Model):
        
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    hexcolor = db.Column(db.String(10))


    
    def __init__(self, name, hexcolor):
            self.name = name
            self.hexcolor = hexcolor
            
    def __repr__(self):
        return self.name