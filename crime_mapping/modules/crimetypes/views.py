from crime_mapping import app
from flask.ext import excel
from werkzeug import secure_filename
from flask import render_template, redirect, flash, url_for, session, abort, request,jsonify
from modules.crimetypes.form import CrimeTypeForm
from crime_mapping import db, uploaded_images
from admin.models import Author
from home.models import Blog, Post, Category, Crime,  Population
from modules.crimetypes.models import CrimeType
from modules.barangays.models import Barangay
from admin.decorators import login_required, author_required,noblog_required,blog_required
import bcrypt, sys
from slugify import slugify
import pygal 
import datetime
from datetime import datetime
from sqlalchemy import extract, func



    

    
@app.route('/admin/manage/crimetypes',methods=('GET','POST'))    
@app.route('/admin/manage/crimetypes/<int:page>',methods=('GET','POST'))
@blog_required
@author_required
def managecrimetypes(page=1):
    form = CrimeTypeForm()
    blog = Blog.query.first()
    
    if form.validate_on_submit():
      
       new_crimetype = CrimeType(form.name.data,form.hexcolor.data.hex)
       db.session.add(new_crimetype)
       db.session.commit()
       flash("New Crime Type has been added!")
       form.name.data=""
       return redirect('admin/manage/crimetypes')
    crimetypes = CrimeType.query.order_by(CrimeType.id.desc()).paginate(page,5,False)
    return render_template('crimetypes/managecrimetypes.html', blog=blog, crimetypes=crimetypes,form=form)
    
    
    
@app.route('/deletecrimetype/<int:crimetype_id>')
@blog_required
@author_required
def deletecrimetype(crimetype_id):
    
    try:
        CrimeType.query.filter_by(id=crimetype_id).delete()
        db.session.commit()
        flash("Crime Type deleted")
        blog = Blog.query.first()
        crimetypes = CrimeType.query.order_by(CrimeType.id.desc()).paginate(page,5,False)
        return render_template('crimetypes/managecrimetype.html', blog=blog, crimetypes=crimetypes,form=form)
    except:
        return redirect('admin/manage/crimetypes')