#from wtforms import Form
from flask_wtf import Form

from crime_mapping import db

from wtforms import validators, StringField, TextAreaField
from wtforms_components import ColorField
from admin.form import RegisterForm
from home.models import Category,  Crime
from modules.crimetypes.models import  CrimeType
from modules.barangays.models import  Barangay
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms import SelectField
from flask_wtf.file import FileField, FileAllowed
from flask_uploads.uploads import UploadSet, configure_uploads, IMAGES
from sqlalchemy import extract



        
class CrimeTypeForm(Form):
    name = StringField('Crime Type Name', [
        validators.Required(),
        validators.Length(max=80)
        ]) 
      
    hexcolor = ColorField('Crime Type Color', [
        validators.Required()
        ])
        
        
