#from wtforms import Form
from flask_wtf import Form

from crime_mapping import db

from wtforms import validators, StringField, TextAreaField,IntegerField,DateField
from wtforms_components import ColorField
from admin.form import RegisterForm
from home.models import Category, Crime,Population
from modules.barangays.models import Barangay
from modules.crimetypes.models import  CrimeType
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms import SelectField
from flask_wtf.file import FileField, FileAllowed
from flask_uploads.uploads import UploadSet, configure_uploads, IMAGES
from sqlalchemy import extract



 
def barangays():
    return Barangay.query   
    
class PopulationForm(Form):

    population = IntegerField('Population', [
        validators.Required(),
        ])     
        
    barangay =  QuerySelectField('Barangay', query_factory=barangays)
    
    year = IntegerField('Year',[
        validators.Required(),
          
        ])   