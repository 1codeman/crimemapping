from crime_mapping import app
from flask.ext import excel
from werkzeug import secure_filename
from flask import render_template, redirect, flash, url_for, session, abort, request,jsonify
from modules.barangays.form import BarangayForm
from crime_mapping import db, uploaded_images
from admin.models import Author
from home.models import Blog, Post, Category, Crime, Population
from modules.crimetypes.models import CrimeType
from modules.barangays.models import Barangay
from admin.decorators import login_required, author_required,noblog_required,blog_required
import bcrypt, sys
from slugify import slugify
import pygal 
import datetime
from datetime import datetime
from sqlalchemy import extract, func


    
@app.route('/admin/manage/barangays',methods=('GET','POST'))    
@app.route('/admin/manage/barangays/<int:page>',methods=('GET','POST'))
@blog_required
@author_required
def managebarangays(page=1):
    form = BarangayForm()
    blog = Blog.query.first()
    
    if form.validate_on_submit():
       new_crimetype = Barangay(form.name.data,form.lat.data,form.lon.data)
       db.session.add(new_crimetype)
       db.session.commit()
       flash("New Barangay has been added!")
       form.name.data=""
       barangays = Barangay.query.order_by(Barangay.name.asc()).paginate(page,5,False)
       return redirect('admin/manage/barangays')
    barangays = Barangay.query.order_by(Barangay.name.asc()).paginate(page,5,False)
    return render_template('barangays/managebarangays.html', blog=blog, barangays=barangays,form=form)
    
    
    
@app.route('/deletebarangay/<int:barangay_id>')
@blog_required
@author_required
def deletebarangay(barangay_id):
    
    try:
        Barangay.query.filter_by(id=barangay_id).delete()
        db.session.commit()
        flash("Barangay deleted")
        blog = Blog.query.first()
        barangays = Barangay.query.order_by(Barangay.name.asc()).paginate(page,5,False)
        return render_template('barangays/managebarangays.html', blog=blog, barangays=barangays,form=form)
    except:
        return redirect('/admin/manage/barangays')