from functools import wraps
from flask import session, request, redirect, url_for, abort
from crime_mapping import app
from crime_mapping import db
from home.models import Blog

#for understanding more about flask view decorators visit:
#http://flask.pocoo.org/docs/0.10/patterns/viewdecorators/
#----------------------------------------------------------


# for page that required to login 
# in order to access the view
def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('username') is None:
            return redirect(url_for('login', next=request.url))
        return f(*args, **kwargs)
    return decorated_function

# for page that required to be logout
# in order to access the view
def notlogin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('username'):
            return redirect(url_for('index'))
        return f(*args, **kwargs)
    return decorated_function

#page will be accessible if no blog/site is in the database
def noblog_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        blog = Blog.query.first()
        if blog:
            return redirect(url_for('index'))
        return f(*args, **kwargs)
    return decorated_function

#page will be accessible if theres a blog/site created
def blog_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        blog = Blog.query.first()
        if not blog:
            return redirect(url_for('setup'))
        return f(*args, **kwargs)
    return decorated_function

#page will only be acessible if the admin/author
# is logged in
def author_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('is_author') is None:
            abort(403)
        return f(*args, **kwargs)
    return decorated_function

