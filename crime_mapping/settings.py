import os

SECRET_KEY = "THIS IS A SECRET_KEY"
DEBUG =False

DB_USERNAME = 'onecodeman'
DB_PASSWORD = ''
DB_CRIME_NAME = 'mapping'
DB_HOST = os.getenv('IP','0.0.0.0')
DB_URI = "mysql+pymysql://%s:%s@%s/%s" % (DB_USERNAME,DB_PASSWORD,DB_HOST,DB_CRIME_NAME)
SQLALCHEMY_DATABASE_URI = DB_URI
SQLALCHEMY_TRACK_MODIFICATIONS = True


POSTS_PER_PAGE = 3

#flask-upload
UPLOADED_IMAGES_DEST = '/home/ubuntu/workspace/crime_mapping/static/images'
UPLOADED_IMAGES_URL = '/static/images/'
